#Justin Henn
#9/16/2018
#CS4500-E01
#This program is a simulation of a pyramid of numbers with multiple iterations
#Python 3.6 used with Spyder and IPython Console
#Sources:
#1. https://stackoverflow.com/questions/850795/different-ways-of-clearing-lists
#2. https://pyformat.info/
#3. https://stackoverflow.com/questions/3996904/generate-random-integers-between-0-and-9
#4. https://stackoverflow.com/questions/23294658/asking-the-user-for-input-until-they-give-a-valid-response

#!/usr/bin/python

from random import randint

#This function is used to get the number of iterations. It asks for a number
#from 10 to 50. If it doesn't get a number or doesn't get a number between
#the specifications then it will ask for the number again. It takes no parameters
# and returns the number of iterations. Source number 4 was used for this
def get_number_of_iters():
    while True:
        try:
            iters = int(input("A number from 10 - 50(inclusive) for layers: "))
        except ValueError:
            print("Please input a number")
            continue
        if iters > 50 or iters < 10 :
            print("Input a valid number from 10 - 50.")
            continue
        else:
            break
    return iters

#This function is used to get the number of layers. It asks for a number
#from 2 to 25. If it doesn't get a number or doesn't get a number between
#the specifications then it will ask for the number again. It takes no parameters
# and returns the number of layers. Source number 4 was used for this

def get_number_of_layers():
    while True:
        try:
            layers = int(input("A number from 2 - 25(inclusive) for repititions: "))
        except ValueError:
            print("Please input a number")
            continue
        if layers > 25 or layers < 2 :
            print("Input a valid number from 2 - 25.")
            continue
        else:
            break
    return layers

#This function gets whether screen output is wanted. It asks for a y or n and if
#it does not receive it then it will ask again. It takes no parameters and returns
#whether it should screen output. Source number 4 was used for this
    
def get_screen_output():
    while True:
        screen_output = input("Do you want to print to screen (y/n): ")
        #print(screen_output)
        if screen_output.lower() == 'y' or screen_output.lower() == 'n':
            break
        else:
            print("Please input y or n.")
            continue
    return screen_output

#This function is used to pick a random movement. 0 = up right, 1 = up left,
#2 = down left, and 3 = down right. The function does not take any parameters.
#The function does return and random integer between 0 and 3. The source
#used for this was number 3.

def random_dice():
        return randint(0, 3)

#This function is used to print information about the game to the screen and to the file.
#The function takes the output file as a parameter and does not return anything.

def print_game_info(outfile):
    
    game_details = ("This is a game where you are asked how many layers you\n"
                    "want in a pyramid. It then asked how many times you want\n"
                    "the game to run. It finally asks if you want output to the\n"
                    "screen. After that a pyramid of numbers 1-how ever many\n"
                    "numbers is need to create the pyramid is created.\n"
                    "The game starts a dot at the number one then randomly will move through\n"
                    "the pyramid going up left, up right, down left, or down right.\n"
                    "If the random movement takes the dot to an invalid place, the\n"
                    "dot will stay where it currently is. The game ends when the dot\n"
                    "has been to every number in the pyramid. The game will display\n"
                    "every movement the dot took on the screen and to the file. After\n"
                    "the dot has visited every place, the game will then print the total\n"
                    "number of movements and the average for each number. It will also\n"
                    "which number/numbers were visited the most. The game will \n"
                    "continue to run for however many iterations you selected at\n"
                    "the beginning. It will then tell you similar totals for the whole run.\n")
    print(game_details)
    outfile.write(game_details)
    outfile.write("\n")

#This function is used to create the pyramid in a list of lists. It first
#initiates counter z and g and h. It then starts a loop that counts to the
#number of layers. It initializes x and a list called b. It then adds whatever
#number g is to the temporary list b. It increments g and x. Until x is equal to z
# It then increments z and h and appends b to the main "list pyramid" It then
#keeps doing this until the full pyramid is created. It takes the list pyramid
#and the total amount of layers as parameters and returns nothing.

def create_list(full_list, total_numbers):
        z = 1
        g = 1
        h = 0

        while h < total_numbers:
                x = 0
                b = []
                while x < z:
                        b.append(g)
                        g += 1
                        x += 1
                z += 1
                h +=1
                full_list.append(b)
                del(b)
                
#This function is used to print the totals of the numbers. It creates a string
# that has the total and average information for all runs in the game. It then
#prints that information. It takes the output file, the total_moves list, the 
#total_maxes list, and the number of iterations as parameters and returns nothing

def print_totals(outfile, total_moves, total_maxes,num_iters):
    total_info = ("Average number of moves: " + str((sum(total_moves))/num_iters)
                    + "\nMiniumum number of moves: " + str(min(total_moves))
                    + "\nMaxmum number of moves: " + str(max(total_moves))
                    + "\nMinimum maximun of dots: " + str(min(total_maxes))
                    + "\nAverage maximum of dots: " + str((sum(total_maxes))/num_iters)
                    + "\nMaximum maximum of dots: " + str(max(total_maxes)))
    print(total_info)
    outfile.write(total_info)

#This function is used to print the totals of the numbers. It creates a string
# that has the total and average information for a run in the game. It then
#prints that information. It takes the output file, the dot_totals list, 
#screen_output, and the full_list list as parameters and returns nothing
    
def print_run(outfile, dot_total, full_list):
    run_info = ("Total number of moves: " + str(sum(dot_total))
                    + "\nAverage number of dots: " + str((sum(dot_total))/full_list[len(full_list)-1][len(full_list)-1])
                    + "\nLargest number of dots: " + str(max(dot_total)) + "\n")
    print(run_info)
    outfile.write(run_info)
    outfile.write("\n")
    
    

#This function is used to print the place where the dot is located. It first
#prints where it is, then it checks to see if it should print a comma or a
#period after. THe period is supposed to be printed if the dot has visited
#every spot, so that is why it checks to see if every dot_total is something
#other than 0. If it is, then that number has been visited. The function takes
#the output file, the full_list pyramid list, the x_coord and y_coord 
#variables used to access the full_list, the dot_total list of times a number
#was visited, and the total_numbers which is the number count in the pyramid.
#The function does not return anything.

def print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output):

    if screen_output.lower() == "y":
        print(full_list[x_coord][y_coord], end='')
        outfile.write("%d" % full_list[x_coord][y_coord])
    print_comma = 0
    for i in range(0, total_numbers):
            if dot_total[i] == 0:
                    print_comma = 1
                    break
    if print_comma == 0:
        if screen_output.lower() == "y":
            print(".\n")
            outfile.write(".\n\n")
    else:
        if screen_output.lower() == "y":
            print(", ", end='')
            outfile.write(", ")

#This funciton is used for the pyramid traversal. It first creates dot_total
#which is a list to keep the total amount of times a number has been visited
#per number. done_checker is used to see if every dot has been visited. The
#first dot_total element is then set to 1 as when the game starts the dot is
#moved to one. The x_coord and y_coord variables are used to traverse the lists
#Right now they are initialized to 0,0 because we are at the number 1 which is
#in the first list, first element, of the full_list. The x_coord shows what list
#we are in, and the y_coord shows what element we are on. It then prints that info.
#Next it checks to see if done_checker is 0. If not then it loops through the
#the dot_total list to see if everything has been visited. If not it will keep
#done_checker set to 0. It then checks to see if done_checker is 0 and if not
#it rolls a random number using the function. It then checks to see if that
#number is 0. If it is then we know we are going up right. THat means we will
#be subtracting one from the x_coord and from the y_coord, since we are going 
#up one list, and back one element. We know if both the x_coord_and y_coord will
#be less than 0 if we do the subtraction that means we cannot move. The if statement
#checks that if the subtraction will be an issue then it just prints the current
#spot we are at since we couldn't move. Else, we update the coords and print where
#we went. Either way we also update the visit totals. If the random number is
#1 then that means up right. That means we subtract 1 from x_coord and we add one
#to y_coord. We check in the if statement to see if minus 1 from the x_coord will
#be less than 0 and if the y_coord is already set to the last element of the list
#we will be going to. If that is so then we can't move and just print where we
#are at. If it okay, then we will subtract one from x_coord and print our new
#location. It will also updates the visit totals as well. It then checks to see
#if the random number is 2. That would mean to move down left. That means moving
#down one list and to the left. It checks to see if x_coord gets updated by one
#if that will go past the amount of lists in the full_list. If that happens then
#the dot stays where it's at. If not then it updates x_coord and prints the new 
#number. It also updates the visit totals. Finally, if the random number is 3
#that means the dot moves down and right. It first checks to see if adding one
#to the x_coord will be out of bounds for the number of lists. If not, then it
#it checks to see if y_coord will be beyond the number of elements in the list
#it is moving to. If either condition is found the it stays where it is at.
#If not then it will move and priunt the new number. It will update visit totals.
#This loop is done until every number has been visited. It then prints the totals.
#The function takes the output file, the full_list pyramid, and the total_numbers
#which represents the number count in the pyramid. The function does not return
#anything.

def pyramid(outfile, full_list, total_numbers, screen_output):
        dot_total = [0] * total_numbers
        return_list = [0] * 2
        done_checker = 0
        dot_total[0] = 1
        x_coord = 0
        y_coord = 0
        print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
        while done_checker == 0:
            for i in range(0, total_numbers):
                if dot_total[i] == 0:
                    done_checker = 0
                    break
                else:
                    done_checker = 1
            if done_checker == 0:
                random_num = random_dice()
                if random_num == 0:
                        if (x_coord - 1) < 0 or (y_coord - 1) < 0:
                                dot_total[full_list[x_coord][y_coord]- 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
                        else:
                                x_coord = x_coord - 1
                                y_coord = y_coord - 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
                elif random_num == 1:
                        if (x_coord - 1) < 0 or y_coord > (len(full_list[x_coord-1]) - 1):
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
                        else:
                                x_coord = x_coord - 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
                elif random_num == 2:
                        if (x_coord + 1) > (len(full_list) - 1):
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
                        else:
                                x_coord = x_coord + 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
                else:
                        if (x_coord + 1) > (len(full_list) - 1) or (y_coord + 1) > (len(full_list[x_coord+1]) - 1):
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)

                        else:
                                x_coord = x_coord + 1
                                y_coord = y_coord + 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers, screen_output)
        print_run(outfile, dot_total, full_list)
        return_list[0] = sum(dot_total)
        return_list[1] = max(dot_total)
        return return_list

#This is the "main portion of the program. It firsts opens the output file.
#It then creates a blank full_list, and sets the total amount of numbers to 21.
#It calls the create_list function and passes the 2 arguments to create the
#pyramid. It then prints the game information using print_game_info. It next
#calls the pyramid function sending the outpfile, the full_list "pyramid",
#and the total_numbers which are how many numbers are in the pyramid.
#After that it closes the file ending the program.
num_layers = get_number_of_layers()
num_iters = get_number_of_iters()
screen_output = get_screen_output()
initoutfile = open("HW3hennOutfile.txt", 'w')
full_list = []
list_of_totals =  [[0,0] for i in range(num_iters)]
total_moves = [0] * num_iters
total_maxes = [0] * num_iters
create_list(full_list, num_layers)
total_numbers = full_list[len(full_list)-1][-1]
print_game_info(initoutfile)
for i in range (0, num_iters):
    list_of_totals[i] = pyramid(initoutfile, full_list, total_numbers, screen_output)
    total_moves[i] = list_of_totals[i][0]
    total_maxes[i] = list_of_totals[i][1]
print_totals(initoutfile, total_moves, total_maxes, num_iters)
initoutfile.close()